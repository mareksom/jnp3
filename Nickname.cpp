#include "Nickname.h"

Nickname::Nickname(Data::Data* data)
    : data_(data),
      title_label_("Enter your nickname:"),
      entry_(),
      age_label_("Age:"),
      age_entry_(),
      sex_label_("Sex:"),
      sex_entry_(),
      cancel_button_("Cancel"),
      ok_button_("Ok") {
  set_margin_top(20);
  set_margin_bottom(20);
  set_margin_left(20);
  set_margin_right(20);
  set_row_spacing(20);
  set_column_spacing(20);

  attach(title_label_, 0, 0, 1, 1);
  attach(entry_, 1, 0, 2, 1);
  attach(age_label_, 0, 1, 1, 1);
  attach(age_entry_, 1, 1, 2, 1);
  attach(sex_label_, 0, 2, 1, 1);
  attach(sex_entry_, 1, 2, 2, 1);
  attach(cancel_button_, 1, 3, 1, 1);
  attach(ok_button_, 2, 3, 1, 1);

  cancel_button_.signal_clicked().connect(
      [this]() -> void {
        signal_done_.emit(Choice::Cancel);
      });

  ok_button_.signal_clicked().connect(
      [this]() -> void {
        std::string new_nickname = entry_.get_buffer()->get_text();
        std::stringstream stream(age_entry_.get_buffer()->get_text());
        int new_age = 0;
        stream >> new_age;
        std::string new_sex = sex_entry_.get_buffer()->get_text();
        data_->nickname() = entry_.get_buffer()->get_text();
        data_->sex() = sex_entry_.get_buffer()->get_text();
        if (!new_nickname.empty() and new_age > 0 and !new_sex.empty()) {
          data_->nickname() = new_nickname;
          data_->age() = new_age;
          data_->sex() = new_sex;
          signal_done_.emit(Choice::Ok);
        }
      });

  age_entry_.signal_changed().connect(
      [this]() -> void {
        std::string text = age_entry_.get_buffer()->get_text();
        std::string new_text = "";
        for (char c : text) {
          if ('0' <= c and c <= '9') {
            new_text += c;
          }
        }
        if ((int) new_text.size() > 3) {
          new_text.resize(3);
        }
        if (new_text != text) {
          age_entry_.set_text(new_text);
        }
      });

  title_label_.show();
  entry_.show();
  age_label_.show();
  age_entry_.show();
  sex_label_.show();
  sex_entry_.show();
  cancel_button_.show();
  ok_button_.show();
}

Nickname::~Nickname() {}

sigc::signal<void, Nickname::Choice> Nickname::signal_done() {
  return signal_done_;
}
