#include "LightSet.h"

#include <cassert>
#include <gtkmm-3.0/gtkmm/messagedialog.h>

#include "AllowLambda.h"
#include "Duck.h"
#include "KeyBindings.h"

LightSet::LightSet(const std::string& title,
                   Data::Session* session,
                   const Data::Configuration* configuration)
    : state_(State::Idle),
      session_(session),
      configuration_(configuration),
      title_label_(title) {
  session_->SetStart(Time::Now());

  title_label_.override_color(Gdk::RGBA("white"));
  attach(title_label_, 0, 0, (Size + 1) / 2, 1);
  attach(countdown_, Size / 2, 0, Size / 2, 1);
  for (int i = 0; i < Size; i++) {
    attach(lights_[i],      i, 1, 1, 1);
    attach(light_labels_[i], i, 2, 1, 1);
  }

  title_label_.show();
  //countdown_.show(); // Should not be visible.
  for (int i = 0; i < Size; i++) {
    light_labels_[i].override_color(Gdk::RGBA("white"));
    light_labels_[i].set_text(configuration_->GetKeyText(i));
    light_labels_[i].show();
    lights_[i].show();
  }

  countdown_.signal_timeout().connect([this]() { on_timeout(); });
}

LightSet::~LightSet() {}

Light::Color LightSet::GetColor(int light_id) const {
  assert(0 <= light_id and light_id < Size);
  return lights_[light_id].GetColor();
}

void LightSet::SetLight(int light_id, Light::Color color) {
  assert(0 <= light_id and light_id < Size);
  lights_[light_id].SetColor(color);
}

void LightSet::SetLights(Light::Color color) {
  for (int i = 0; i < Size; i++) {
    SetLight(i, color);
  }
}

void LightSet::SetLights(const Data::Pattern& pattern) {
  for (int i = 0; i < Size; i++) {
    if (pattern.get(i)) {
      lights_[i].SetColor(Light::Color::Yellow);
    } else {
      lights_[i].SetColor(Light::Color::None);
    }
  }
}

bool LightSet::RunRound() {
  if (state_ != State::Idle) {
    return false;
  }
  state_ = State::Round;
  pattern_result_ = Data::PatternResult();
  pattern_result_.SetStart(Time::Now());
  Data::Pattern pattern = session_->RandomPattern();
  pattern_result_.SetPattern(pattern);
  SetLights(pattern);
  if (configuration_->is_deadline()) {
    countdown_.SetTo(configuration_->deadline_ms());
  } else {
    countdown_.Reset();
  }
  return true;
}

bool LightSet::Trigger(int light_id) {
  assert(0 <= light_id and light_id < Size);
  if (state_ != State::Round) {
    return false;
  }
  pattern_result_.AddKeyPress(Time::Now(), light_id);
  if (GetColor(light_id) == Light::Color::Yellow) {
    SetLight(light_id, Light::Color::None);
  } else if (configuration_->feedback()) {
    SetLight(light_id, Light::Color::Red);
    Duck::Play();
  }
  if (RoundIsDone()) {
    RoundSummary();
  }
  return true;
}

bool LightSet::RoundIsDone() const {
  if (state_ != State::Round) {
    return false;
  }
  for (int i = 0; i < Size; i++) {
    if (GetColor(i) == Light::Color::Yellow) {
      return false;
    }
  }
  return true;
}

void LightSet::Reset() {
  countdown_.Reset();
  SetLights(Light::Color::None);
  state_ = State::Idle;
}

sigc::signal<void> LightSet::signal_end_of_round() {
  return signal_end_of_round_;
}

bool LightSet::on_draw(const Cairo::RefPtr<Cairo::Context>& context) {
  context->save();
    context->set_source_rgb(0, 0, 0);
    context->paint();
  context->restore();
  return Gtk::Grid::on_draw(context);
}

void LightSet::RoundSummary() {
  assert(state_ == State::Round);
  pattern_result_.SetEnd(Time::Now());
  session_->AddPatternResult(pattern_result_);
  session_->SetEnd(Time::Now());
  countdown_.Stop();
  state_ = State::Idle;

  // Setting red and green lights.
  SetLights(pattern_result_.pattern());
  pattern_result_.IterateKeys(
      [this](Time::Duration time, int key) -> void {
        assert(0 <= key and key < Size);
        if (GetColor(key) == Light::Color::Yellow) {
          SetLight(key, Light::Color::None);
        } else {
          SetLight(key, Light::Color::Red);
        }
      });
  for (int i = 0; i < Size; i++) {
    if (GetColor(i) == Light::Color::None) {
      SetLight(i, Light::Color::Green);
    } else {
      SetLight(i, Light::Color::Red);
    }
  }

  signal_end_of_round_.emit();
}

void LightSet::on_timeout() {
  assert(state_ == State::Round);
  RoundSummary();
}
