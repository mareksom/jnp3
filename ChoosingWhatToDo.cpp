#include "ChoosingWhatToDo.h"

ChoosingWhatToDo::ChoosingWhatToDo()
    : solve_button_("Solve"),
      view_button_("View"),
      back_button_("Back"),
      grid_() {
  grid_.set_row_spacing(20);
  grid_.set_column_spacing(20);
  grid_.set_margin_top(20);
  grid_.set_margin_bottom(20);
  grid_.set_margin_left(20);
  grid_.set_margin_right(20);

  grid_.attach(back_button_, 0, 0, 1, 1);
  grid_.attach(solve_button_, 1, 0, 1, 1);
  grid_.attach(view_button_, 2, 0, 1, 1);

  add(grid_);

  solve_button_.show();
  view_button_.show();
  back_button_.show();
  grid_.show();

  solve_button_.signal_clicked().connect(
      [this]() -> void {
        signal_.emit(Choice::Solve);
      });
  view_button_.signal_clicked().connect(
      [this]() -> void {
        signal_.emit(Choice::View);
      });
  back_button_.signal_clicked().connect(
      [this]() -> void {
        signal_.emit(Choice::Back);
      });
}

ChoosingWhatToDo::~ChoosingWhatToDo() {}

sigc::signal<void, ChoosingWhatToDo::Choice> ChoosingWhatToDo::signal_done() {
  return signal_;
}
