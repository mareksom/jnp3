#ifndef DUCK_H_
#define DUCK_H_

namespace Duck {

void Init(int argc, char** argv);
void Play();
void Quit();

}  // namespace Duck

#endif  // DUCK_H_
