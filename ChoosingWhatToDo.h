#ifndef CHOOSING_WHAT_TO_DO_H_
#define CHOOSING_WHAT_TO_DO_H_

#include <gtkmm-3.0/gtkmm/bin.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/grid.h>

class ChoosingWhatToDo : public Gtk::Grid {
 public:
  ChoosingWhatToDo();
  virtual ~ChoosingWhatToDo();

  enum class Choice {
    Solve,
    View,
    Back,
  };

  sigc::signal<void, Choice> signal_done();

 private:
  Gtk::Button solve_button_;
  Gtk::Button view_button_;
  Gtk::Button back_button_;
  Gtk::Grid grid_;

  sigc::signal<void, Choice> signal_;
};

#endif  // CHOOSING_WHAT_TO_DO_H_
