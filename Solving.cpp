#include "Solving.h"

#include <cassert>

#include "AllowLambda.h"

Solving::Solving(const std::string& title, Data::Session* session,
                 const Data::Configuration* configuration)
    : configuration_(configuration),
      light_set_(title, session, configuration) {
  add(light_set_);
  light_set_.show();

  add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK);

  light_set_.signal_end_of_round().connect(
      [this]() -> void {
        signal_.emit(Event::EndOfRound);
      });

  set_can_focus(true);
}

Solving::~Solving() {
  timeout_connection_.disconnect();
}

void Solving::StartRound() {
  timeout_connection_ = Glib::signal_timeout().connect(
      [this]() -> bool {
          light_set_.SetLights(Light::Color::Blue);
          timeout_connection_ = Glib::signal_timeout().connect(
              [this]() -> bool {
                assert(light_set_.RunRound());
                return false;
              }, (configuration_->delay_ms() + 1) / 2);
        return false;
      }, configuration_->delay_ms() / 2);
}

sigc::signal<void, Solving::Event> Solving::signal_event() {
  return signal_;
}

bool Solving::on_key_press_event(GdkEventKey* key_event) {
  int which = configuration_->WhichPressed(key_event->keyval);
  if (which != -1) {
    light_set_.Trigger(which);
    return true;
  } else {
    if (key_event->keyval == GDK_KEY_Escape) {
      light_set_.Reset();
      timeout_connection_.disconnect();
      signal_.emit(Event::Escape);
      return true;
    }
  }
  return false;
}
