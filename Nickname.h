#ifndef NICKNAME_H_
#define NICKNAME_H_

#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/entry.h>
#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/label.h>

#include "data/Data.h"

class Nickname : public Gtk::Grid {
 public:
  Nickname(Data::Data* data);
  virtual ~Nickname();

  enum class Choice {
    Ok, Cancel
  };

  sigc::signal<void, Choice> signal_done();

 private:
  Data::Data* data_;

  Gtk::Label title_label_;
  Gtk::Entry entry_;
  Gtk::Label age_label_;
  Gtk::Entry age_entry_;
  Gtk::Label sex_label_;
  Gtk::Entry sex_entry_;
  Gtk::Button cancel_button_;
  Gtk::Button ok_button_;

  sigc::signal<void, Choice> signal_done_;
};

#endif  // NICKNAME_H_
