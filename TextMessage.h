#ifndef TEXT_MESSAGE_H_
#define TEXT_MESSAGE_H_

#include <gtkmm-3.0/gtkmm/bin.h>
#include <gtkmm-3.0/gtkmm/box.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/textview.h>
#include <string>

class TextMessage : public Gtk::Bin {
 public:
  TextMessage(const std::string& message);

  sigc::signal<void> signal_done();

 private:
  Gtk::Box v_box_;
  Gtk::TextView message_;
  Gtk::Button ok_button_;

  sigc::signal<void> signal_done_;
};

#endif  // TEXT_MESSAGE_H_
