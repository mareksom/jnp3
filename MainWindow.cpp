#include "MainWindow.h"

#include <cassert>
#include <gtkmm-3.0/gtkmm/messagedialog.h>

#include "AllowLambda.h"

MainWindow::MainWindow() {
  set_default_size(800, 600);
  set_title("1023");
  set_can_focus(false);
  LoadData();
}

MainWindow::~MainWindow() {}

void MainWindow::LoadData() {
  loading_data_ = std::unique_ptr<LoadingData>(new LoadingData(&data_));
  add(*loading_data_);
  loading_data_->signal_done().connect(
      [this]() -> void {
        loading_data_.reset();
        ChooseWhatToDo();
      });
  loading_data_->show();
  Glib::signal_timeout().connect_once(
      [this]() -> void {
        loading_data_->grab_focus();
      }, 100);
}

void MainWindow::ChooseWhatToDo() {
  choosing_what_to_do_
      = std::unique_ptr<ChoosingWhatToDo>(new ChoosingWhatToDo());
  add(*choosing_what_to_do_);
  choosing_what_to_do_->signal_done().connect(
      [this](ChoosingWhatToDo::Choice choice) -> void {
        choosing_what_to_do_.reset();
        if (choice == ChoosingWhatToDo::Choice::Back) {
          LoadData();
        } else if (choice == ChoosingWhatToDo::Choice::Solve) {
          RunASession();
        } else if (choice == ChoosingWhatToDo::Choice::View) {
          RunChoosingResults();
        } else {
          assert(false);
        }
      });
  choosing_what_to_do_->show();
}

void MainWindow::RunASession() {
  if (data_.nickname() == "") {
    RunNickname();
  } else if (data_.start_with_training()) {
    RunTrainingSession();
  } else {
    RunTestSession();
  }
}

void MainWindow::RunNickname() {
  nickname_ = std::unique_ptr<Nickname>(new Nickname(&data_));
  add(*nickname_);
  nickname_->signal_done().connect(
      [this](Nickname::Choice choice) -> void {
        nickname_.reset();
        data_.Save();
        if (choice == Nickname::Choice::Ok) {
          RunASession();
        } else if (choice == Nickname::Choice::Cancel) {
          ChooseWhatToDo();
        } else {
          assert(false);
        }
      });
  nickname_->show();
}

void MainWindow::RunTrainingSession() {
  assert(data_.start_with_training());
  training_session_
      = std::unique_ptr<TrainingSession>(new TrainingSession(&data_));
  add(*training_session_);
  training_session_->signal_done().connect(
      [this](TrainingSession::Choice choice) -> void {
        training_session_.reset();
        if (choice == TrainingSession::Choice::Next) {
          data_.start_with_training() = false;
          RunTestSession();
        } else if (choice == TrainingSession::Choice::Back) {
          data_.Save();
          ChooseWhatToDo();
        } else {
          assert(false);
        }
      });
  training_session_->show();
}

void MainWindow::RunTestSession() {
  assert(!data_.start_with_training());
  test_session_ = std::unique_ptr<TestSession>(new TestSession(&data_));
  add(*test_session_);
  test_session_->signal_done().connect(
      [this](TestSession::Choice choice) -> void {
        test_session_.reset();
        if (choice == TestSession::Choice::Back) {
          data_.Save();
          ChooseWhatToDo();
        } else if (choice == TestSession::Choice::End) {
          data_.Save();
          Gtk::MessageDialog dialog(*this, "The test is done.");
          dialog.set_secondary_text(
              "You have completed all "
              + std::to_string(data_.configuration().test_session_size())
              + " * 1023 patterns!");
          dialog.run();
          ChooseWhatToDo();
        } else {
          assert(false);
        }
      });
  test_session_->show();
}

void MainWindow::RunChoosingResults() {
  choosing_results_
      = std::unique_ptr<ChoosingResults>(new ChoosingResults(&data_));
  add(*choosing_results_);
  choosing_results_->signal_done().connect(
      [this]() -> void {
        choosing_results_.reset();
        ChooseWhatToDo();
      });
  choosing_results_->show();
}
