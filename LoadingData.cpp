#include "LoadingData.h"

#include <cassert>
#include <gtkmm-3.0/gtkmm/filechooserdialog.h>

#include <unistd.h>

LoadingData::LoadingData(Data::Data* data)
    : password_("kluski"),
      matched_(0),
      state_(State::Idle),
      worker_(nullptr),
      configuration_(nullptr),
      load_button_("Load session from file"),
      new_button_("Start new session"),
      message_("Message"),
      grid_(),
      data_(data) {
  add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK);
  set_can_focus(true);

  grid_.set_row_spacing(20);
  grid_.set_column_spacing(20);
  grid_.set_margin_top(20);
  grid_.set_margin_bottom(20);
  grid_.set_margin_left(20);
  grid_.set_margin_right(20);

  grid_.attach(new_button_, 0, 0, 1, 1);
  grid_.attach(load_button_, 1, 0, 1, 1);
  grid_.attach(message_, 0, 2, 2, 1);

  add(grid_);

  load_button_.show();
  //new_button_.show();
  grid_.show();

  load_button_.signal_clicked().connect([this]() -> void { LoadData(); });
  new_button_.signal_clicked().connect([this]() -> void { NewSession(); });

  dispatcher_.connect([this]() { on_notification(); });
}

LoadingData::~LoadingData() {}

sigc::signal<void> LoadingData::signal_done() {
  return signal_done_;
}

bool LoadingData::on_key_press_event(GdkEventKey* key_event) {
  const guint keyval = gdk_keyval_to_lower(key_event->keyval);
  const std::string name = gdk_keyval_name(keyval);
  if (!name.empty()) {
    char c = name[0];
    if ('a' <= c and c <= 'z') {
      auto is_prefix = [this](const std::string& text) -> bool {
        if (text.size() > password_.size()) {
          return false;
        }
        for (int i = 0; i < (int) text.size(); i++) {
          if (password_[i] != text[i]) {
            return false;
          }
        }
        return true;
      };
      assert(0 <= matched_ and matched_ <= (int) password_.size());
      std::string match = password_.substr(0, matched_) + c;
      while (!is_prefix(match)) {
        match = match.substr(1);
      }
      matched_ = (int) match.size();
      if (matched_ == (int) password_.size()) {
        NewSession();
      }
      return true;
    }
  }
  return false;
}

void LoadingData::NewSession() {
  if (state_ != State::Idle) {
    return;
  }
  *data_ = Data::Data();
  state_ = State::Configuration;
  grid_.hide();
  remove();
  configuration_ = std::unique_ptr<Configuration>(
      new Configuration(data_));
  add(*configuration_);
  configuration_->show();
  configuration_->signal_event().connect(
      [this](Configuration::Event event) -> void {
        configuration_.reset();
        if (event == Configuration::Event::Back) {
          state_ = State::Idle;
          add(grid_);
          grid_.show();
          grab_focus();
        } else if (event == Configuration::Event::Done) {
          signal_done_.emit();
        } else {
          assert(false);
        }
      });
}

void LoadingData::LoadData() {
  if (state_ != State::Idle) {
    return;
  }

  // Creating the dialog.
  Gtk::FileChooserDialog dialog("Choose file", Gtk::FILE_CHOOSER_ACTION_OPEN);

  dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("_Open", Gtk::RESPONSE_OK);

  auto filter = Gtk::FileFilter::create();
  filter->set_name("MS files");
  filter->add_pattern("*.ms");
  dialog.add_filter(filter);

  int result = dialog.run();

  if (result == Gtk::RESPONSE_OK) {
    state_ = State::LoadingData;
    message_.set_text("Loading...");
    message_.show();
    error_message_ = "";

    const std::string filename = dialog.get_filename();

    worker_ = std::unique_ptr<std::thread>(new std::thread(
        [this, filename]() -> void {
          sleep(1);
          try {
            *data_ = Data::Data::ReadFrom(filename);
          } catch (const std::runtime_error& e) {
            error_message_ = std::string("Error: ") + e.what();
          }
          dispatcher_.emit();
        }));
    grid_.set_sensitive(false);
  } else {
    message_.set_text("Cancelled.");
    message_.show();
  }
}

void LoadingData::on_notification() {
  assert(worker_ != nullptr);
  worker_->join();
  worker_.reset();
  assert(worker_ == nullptr);
  grid_.set_sensitive(true);
  if (error_message_ == "") {
    signal_done_.emit();
  } else {
    state_ = State::Idle;
    message_.set_text(error_message_);
  }
}
