#include "Light.h"

Light::Light() : width_(80), height_(80), color_(Light::Color::None) {}

Light::~Light() {}

void Light::SetColor(Color color) {
  color_ = color;
  queue_draw();
}

Light::Color Light::GetColor() const {
  return color_;
}

void Light::Resize(int size) {
  width_ = height_ = size;
  queue_resize();
}

Gtk::SizeRequestMode Light::get_request_mode_vfunc() const {
  return Gtk::SIZE_REQUEST_CONSTANT_SIZE;
}

void Light::get_preferred_width_vfunc(
    int& minimum_width, int& natural_width) const {
  minimum_width = width_;
  natural_width = width_;
}

void Light::get_preferred_height_for_width_vfunc(
    int width, int& minimum_height, int& natural_height) const {
  minimum_height = width;
  natural_height = width;
}

void Light::get_preferred_height_vfunc(
    int& minimum_height, int& natural_height) const {
  minimum_height = height_;
  natural_height = height_;
}

void Light::get_preferred_width_for_height_vfunc(
    int height, int& minimum_width, int& natural_width) const {
  minimum_width = height;
  natural_width = height;
}

void Light::on_size_allocate(Gtk::Allocation& allocation) {
  set_allocation(allocation);
  if (window_) {
    window_->move_resize(allocation.get_x(), allocation.get_y(),
                         allocation.get_width(), allocation.get_height());
  }
}

void Light::on_realize() {
  set_realized();
  if (!window_) {
    GdkWindowAttr attributes;
    memset(&attributes, 0, sizeof(attributes));

    Gtk::Allocation allocation = get_allocation();
    attributes.x = allocation.get_x();
    attributes.y = allocation.get_y();
    attributes.width = allocation.get_width();
    attributes.height = allocation.get_height();

    attributes.event_mask = get_events() | Gdk::EXPOSURE_MASK;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.wclass = GDK_INPUT_OUTPUT;

    window_ = Gdk::Window::create(get_parent_window(), &attributes,
                                  GDK_WA_X | GDK_WA_Y);
    set_window(window_);

    window_->set_user_data(gobj());
  }
}

void Light::on_unrealize() {
  window_.reset();
  Gtk::Widget::on_unrealize();
}

bool Light::on_draw(const Cairo::RefPtr<Cairo::Context>& context) {
  // Black background.
  context->save();
    context->set_source_rgb(0, 0, 0);
    context->paint();
  context->restore();

  // Light.
  auto add_color = [](Cairo::RefPtr<Cairo::RadialGradient>& gradient,
                      double point, int R, int G, int B, double A) {
    gradient->add_color_stop_rgba(point, R / 255., G / 255., B / 255., A);
  };
  context->save();
    const Gtk::Allocation allocation = get_allocation();
    context->scale(allocation.get_width() / 2, allocation.get_height() / 2);
    context->translate(1.0, 1.0);

    // Drawing a light / a bulb.
    context->save();
      auto light_gradient
          = Cairo::RadialGradient::create(0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
      if (color_ == Color::Yellow) {
        add_color(light_gradient, 0.0, 255, 254, 179, 1.0);
        add_color(light_gradient, 0.1, 254, 254, 168, 1.0);
        add_color(light_gradient, 0.2, 254, 242, 149, 1.0);
        add_color(light_gradient, 0.3, 251, 237, 140, 1.0);
        add_color(light_gradient, 0.4, 252, 220, 121, 1.0);
        add_color(light_gradient, 0.44, 247, 217, 105, 1.0);
        add_color(light_gradient, 0.48, 246, 204, 101, 1.0);
        add_color(light_gradient, 0.5, 230, 193, 79, 1.0);
        add_color(light_gradient, 0.52, 191, 146, 29, 1.0);
        add_color(light_gradient, 0.55, 171, 120, 2, 1.0);
        add_color(light_gradient, 0.6, 150, 106, 0, 1.0);
        add_color(light_gradient, 0.75, 75, 49, 0, 1.0);
        add_color(light_gradient, 1.0, 0, 0, 0, 0.0);
      } else if (color_ == Color::Red) {
        add_color(light_gradient, 0.0, 255, 105, 99, 1.0);
        add_color(light_gradient, 0.1, 255, 90, 89, 1.0);
        add_color(light_gradient, 0.2, 255, 73, 73, 1.0);
        add_color(light_gradient, 0.3, 255, 60, 63, 1.0);
        add_color(light_gradient, 0.4, 255, 49, 49, 1.0);
        add_color(light_gradient, 0.44, 255, 31, 36, 1.0);
        add_color(light_gradient, 0.48, 255, 27, 38, 1.0);
        add_color(light_gradient, 0.5, 252, 0, 18, 1.0);
        add_color(light_gradient, 0.52, 178, 20, 37, 1.0);
        add_color(light_gradient, 0.55, 140, 0, 16, 1.0);
        add_color(light_gradient, 0.6, 122, 0, 14, 1.0);
        add_color(light_gradient, 0.75, 60, 0, 10, 1.0);
        add_color(light_gradient, 1.0, 0, 0, 0, 0.0);
      } else if (color_ == Color::Blue) {
        add_color(light_gradient, 0.0, 193, 190, 255, 1.0);
        add_color(light_gradient, 0.1, 181, 181, 255, 1.0);
        add_color(light_gradient, 0.2, 167, 179, 255, 1.0);
        add_color(light_gradient, 0.3, 159, 170, 255, 1.0);
        add_color(light_gradient, 0.4, 135, 166, 255, 1.0);
        add_color(light_gradient, 0.44, 129, 160, 255, 1.0);
        add_color(light_gradient, 0.48, 117, 155, 255, 1.0);
        add_color(light_gradient, 0.5, 109, 147, 255, 1.0);
        add_color(light_gradient, 0.52, 0, 46, 162, 1.0);
        add_color(light_gradient, 0.55, 0, 39, 128, 1.0);
        add_color(light_gradient, 0.6, 0, 34, 110, 1.0);
        add_color(light_gradient, 0.75, 0, 20, 56, 1.0);
        add_color(light_gradient, 1.0, 0, 0, 0, 0.0);
      } else if (color_ == Color::Green) {
        add_color(light_gradient, 0.0, 195, 255, 53, 1.0);
        add_color(light_gradient, 0.1, 186, 255, 45, 1.0);
        add_color(light_gradient, 0.2, 170, 255, 33, 1.0);
        add_color(light_gradient, 0.3, 160, 255, 23, 1.0);
        add_color(light_gradient, 0.4, 149, 255, 11, 1.0);
        add_color(light_gradient, 0.44, 136, 250, 0, 1.0);
        add_color(light_gradient, 0.48, 121, 238, 0, 1.0);
        add_color(light_gradient, 0.5, 114, 220, 0, 1.0);
        add_color(light_gradient, 0.52, 77, 156, 0, 1.0);
        add_color(light_gradient, 0.55, 64, 124, 0, 1.0);
        add_color(light_gradient, 0.6, 55, 106, 0, 1.0);
        add_color(light_gradient, 0.75, 30, 54, 0, 1.0);
        add_color(light_gradient, 1.0, 0, 0, 0, 0.0);
      } else if (color_ == Color::None) {
        add_color(light_gradient, 0.0, 6, 6, 6, 1.0);
        add_color(light_gradient, 0.1, 10, 10, 10, 1.0);
        add_color(light_gradient, 0.2, 10, 10, 10, 1.0);
        add_color(light_gradient, 0.3, 8, 8, 8, 1.0);
        add_color(light_gradient, 0.4, 13, 13, 13, 1.0);
        add_color(light_gradient, 0.45, 33, 33, 33, 1.0);
        add_color(light_gradient, 0.48, 49, 49, 47, 1.0);
        add_color(light_gradient, 0.5, 52, 52, 50, 1.0);
        add_color(light_gradient, 0.55, 0, 0, 0, 0.0);
      }
      context->set_source(light_gradient);
      context->paint();
    context->restore();
    if (color_ == Color::None) {
      for (int j = 0; j < 2; j++) {
        for (int i = 0; i < 16; i++) {
          context->save();
            if (j == 1) {
              context->scale(-1.0, -1.0);
            }
            context->rotate(-M_PI / (i * 0.25 + 1));
            context->translate(-0.15 + 0.0125 * i, 0.03 - i * 0.00125);
            auto grad = Cairo::RadialGradient::create(
                0.0, -0.3, 0.3, 0.0, 0.0, 0.3);
            add_color(grad, 0.0, 0, 0, 0, 0.0);
            add_color(grad, 0.5, 70, 70, 70, 0.05);
            add_color(grad, 1.0, 0, 0, 0, 0.0);
            context->set_source(grad);
            context->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
            context->fill();
          context->restore();
        }
        for (int i = 0; i < 8; i++) {
          context->save();
            if (j == 1) {
              context->scale(-1.0, -1.0);
            }
            context->rotate(-M_PI / (i * 0.5 + 1));
            context->translate(-0.15 + 0.025 * i, 0.03 - i * 0.0025);
            auto grad = Cairo::RadialGradient::create(
                0.0, -0.3, 0.3, 0.0, 0.0, 0.3);
            add_color(grad, 0.0, 0, 0, 0, 0.0);
            add_color(grad, 0.5, 70, 70, 70, 0.05);
            add_color(grad, 1.0, 0, 0, 0, 0.0);
            context->set_source(grad);
            context->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
            context->fill();
          context->restore();
        }
      }
    }
  context->restore();
  return true;
}
