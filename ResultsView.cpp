#include "ResultsView.h"

#include "AllowLambda.h"

ResultsView::ResultsView(const Data::Session* session)
    : Gtk::Box(Gtk::ORIENTATION_HORIZONTAL),
      box_(Gtk::ORIENTATION_VERTICAL),
      back_button_("Back"),
      results_list_(session) {
  pack_start(box_, Gtk::PACK_SHRINK);
  pack_end(pattern_result_view_, Gtk::PACK_EXPAND_WIDGET);

  box_.pack_start(back_button_, Gtk::PACK_SHRINK);
  box_.pack_end(results_list_, Gtk::PACK_EXPAND_WIDGET);

  back_button_.set_margin_bottom(20);
  set_margin_top(20);
  set_margin_bottom(20);
  set_margin_left(20);
  set_margin_right(20);

  results_list_.signal_pattern_result_selected().connect(
      [this](const Data::PatternResult* pattern_result) -> void {
        pattern_result_view_.SetPatternResult(pattern_result);
      });

  back_button_.signal_clicked().connect(
      [this]() -> void {
        signal_done_.emit();
      });

  back_button_.show();
  results_list_.show();
  box_.show();
  pattern_result_view_.show();
}

ResultsView::~ResultsView() {}

sigc::signal<void> ResultsView::signal_done() {
  return signal_done_;
}
