#include <glibmm/refptr.h>
#include <gtkmm-3.0/gtkmm/application.h>
#include <gtkmm-3.0/gtkmm/window.h>

#include "Duck.h"
#include "MainWindow.h"

int main(int argc, char** argv) {
  Duck::Init(argc, argv);

  argc = 1;  // I lie to the application, because it's too stupid
             // to accept a code without those arguments.
  Glib::RefPtr<Gtk::Application> application
      = Gtk::Application::create(argc, argv);

  MainWindow window;
  application->run(window);

  Duck::Quit();
  return 0;
}
