#ifndef SOLVING_H_
#define SOLVING_H_

#include <gtkmm-3.0/gtkmm/eventbox.h>

#include "LightSet.h"
#include "data/Configuration.h"
#include "data/Session.h"

class Solving : public Gtk::EventBox {
 public:
  Solving(const std::string& title, Data::Session* session,
          const Data::Configuration* configuration);
  virtual ~Solving();

  void StartRound();

  enum class Event {
    EndOfRound,
    Escape,
  };

  sigc::signal<void, Event> signal_event();

 protected:
  bool on_key_press_event(GdkEventKey* key_event) override;

 private:
  const Data::Configuration* configuration_;

  sigc::connection timeout_connection_;

  LightSet light_set_;

  sigc::signal<void, Event> signal_;
};

#endif  // SOLVING_H_
