#ifndef LIGHT_H_
#define LIGHT_H_

#include <gtkmm-3.0/gtkmm/widget.h>

class Light : public Gtk::Widget {
 public:
  Light();
  virtual ~Light();

  enum class Color {
    Yellow, Red, Blue, Green, None
  };

  void SetColor(Color color);
  Color GetColor() const;

  void Resize(int size);

 protected:
  Gtk::SizeRequestMode get_request_mode_vfunc() const override;
  void get_preferred_width_vfunc(
      int& minimium_width, int& natural_width) const override;
  void get_preferred_height_for_width_vfunc(
      int width, int& minimum_height, int& natural_height) const override;
  void get_preferred_height_vfunc(
      int& minimum_height, int& natural_height) const override;
  void get_preferred_width_for_height_vfunc(
      int height, int& minimum_width, int& natural_width) const override;
  void on_size_allocate(Gtk::Allocation& allocation) override;
  void on_realize() override;
  void on_unrealize() override;
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& context) override;

  Glib::RefPtr<Gdk::Window> window_;

 private:
  int width_;
  int height_;

  Color color_;
};

#endif  // LIGHT_H_
