BUILD = build
CPP = clang++
CPPFLAGS = -pthread -std=c++11 -g `pkg-config gtkmm-3.0 --cflags` -Wno-deprecated-register -Wno-unknown-warning-option
LDFLAGS = `pkg-config gtkmm-3.0 --cflags --libs` -lSDL -lSDL_mixer
OBJS = Main.o MainWindow.o Configuration.o LightSet.o Light.o Countdown.o LoadingData.o ChoosingWhatToDo.o TrainingSession.o Solving.o KeyBindings.o TestSession.o \
       ResultsView.o ResultsList.o ResultsListRow.o PatternResultView.o ChoosingResults.o Nickname.o Duck.o TextMessage.o
HEADERS = data/Time.h data/Data.h data/Pattern.h data/PatternResult.h data/Session.h data/Configuration.h \
          MainWindow.h Configuration.h LightSet.h Light.h Countdown.h LoadingData.h ChoosingWhatToDo.h TrainingSession.h Solving.h KeyBindings.h TestSession.h \
          ResultsView.h ResultsList.h ResultsListRow.h PatternResultView.h ChoosingResults.h Nickname.h Duck.h TextMessage.h

all: $(BUILD)/run

$(BUILD)/run: $(addprefix $(BUILD)/, $(OBJS)) | $(BUILD)
	$(CPP) $^ -o $@ $(LDFLAGS)

$(BUILD)/%.o: %.cpp $(HEADERS) | $(BUILD)
	$(CPP) -c $< -o $@ $(CPPFLAGS)

$(BUILD):
	mkdir -p $(BUILD)

.PHONY: clean

clean:
	rm -rf $(BUILD)
