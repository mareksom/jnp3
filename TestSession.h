#ifndef TEST_SESSION_H_
#define TEST_SESSION_H_

#include <gtkmm-3.0/gtkmm/bin.h>

#include "Solving.h"
#include "TextMessage.h"
#include "data/Configuration.h"
#include "data/Data.h"
#include "data/Session.h"

class TestSession : public Gtk::Bin {
 public:
  TestSession(Data::Data* data);
  virtual ~TestSession();

  enum class Choice {
    End,
    Back,
  };

  sigc::signal<void, Choice> signal_done();

 private:
  void SetupSolving();
  void Start();

  bool End() const;

  Data::Session* test_session_;
  Data::Configuration* configuration_;

  Solving solving_;
  std::unique_ptr<TextMessage> text_message_;

  sigc::signal<void, Choice> signal_;
};

#endif  // TEST_SESSION_H_
