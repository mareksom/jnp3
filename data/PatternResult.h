#ifndef DATA_PATTERN_RESULT_H_
#define DATA_PATTERN_RESULT_H_

#include <iostream>
#include <string>
#include <vector>

#include "Pattern.h"
#include "Time.h"

namespace Data {

class PatternResult {
 public:
  Pattern pattern() const { return pattern_; }
  Time::TimePoint start() const { return start_; }
  Time::TimePoint end() const { return end_; }

  // Iterates over all keys that were pressed on this pattern.
  // Callback: Time::Duration -> int -> void
  template <typename Callback>
  void IterateKeys(Callback callback) const {
    for (const auto& p : keys_) {
      callback(p.first, p.second);
    }
  }

  void AddKeyPress(const Time::Duration& time, int key) {
    keys_.emplace_back(time, key);
  }

  void AddKeyPress(const Time::TimePoint& time, int key) {
    AddKeyPress(time - start_, key);
  }

  void SetStart(const Time::TimePoint& time) {
    start_ = time;
  }

  void SetEnd(const Time::TimePoint& time) {
    end_ = time;
  }

  void SetPattern(const Pattern& pattern) {
    pattern_ = pattern;
  }

  void Write(std::ostream& stream) const {
    pattern_.Write(stream); stream << "\n";
    Time::Write(start_, stream); stream << "\n";
    Time::Write(end_, stream); stream << "\n";
    stream << keys_.size() << "\n";
    for (const auto& k : keys_) {
      Time::Write(k.first, stream);
      stream << " " << k.second << "\n";
    }
  }

  void Read(std::istream& stream) {
    PatternResult tmp;
    tmp.pattern_.Read(stream);
    Time::Read(tmp.start_, stream);
    Time::Read(tmp.end_, stream);
    int number_of_keys = 0;
    stream >> number_of_keys;
    for (int i = 0; i < number_of_keys; i++) {
      Time::Duration time;
      int key;
      Time::Read(time, stream);
      stream >> key;
      tmp.keys_.emplace_back(time, key);
    }
    if (stream) {
      std::swap(*this, tmp);
    }
  }

  void WriteCsv(std::ostream& stream) const {
    stream << "Pattern:" << ",";
    pattern_.WriteCsv(stream);
    stream << "," << "Start:" << "," << Time::ToString(start_);
    stream << "," << "End:" << "," << Time::ToString(end_) << ",";
    IterateKeys(
        [this, &stream](const Time::Duration& duration, int key) -> void {
          stream << "," << "Key " << key << ","
                 << "+" << Time::ToMilliseconds(duration);
        });
    stream << "\r\n";
  }

 private:
  Pattern pattern_;
  Time::TimePoint start_, end_;
  std::vector<std::pair<Time::Duration, int>> keys_;
};

}  // namespace Data

#endif  // DATA_PATTERN_RESULT_H_
