#ifndef DATA_TIME_H_
#define DATA_TIME_H_

#include <chrono>
#include <ctime>
#include <iostream>
#include <string>

namespace Time {
  typedef std::chrono::system_clock Clock;
  typedef std::chrono::time_point<Time::Clock> TimePoint;
  typedef Time::Clock::duration Duration;

  inline Time::TimePoint Now() {
    return Time::Clock::now();
  }

  inline int ToMilliseconds(const Time::Duration& duration) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
        duration).count();
  }

  inline std::string ToString(const Time::TimePoint& time) {
    std::time_t unix_time = Time::Clock::to_time_t(time);
    char buffer[30];
    if (!std::strftime(buffer, 30, "%F %T", std::localtime(&unix_time))) {
      return "(?!?)";
    }
    return std::string(buffer);
  }

  inline void Write(const Time::TimePoint& time, std::ostream& stream) {
    std::time_t unix_time = Time::Clock::to_time_t(time);
    stream << unix_time;
  }

  inline void Write(const Time::Duration& duration, std::ostream& stream) {
    auto duration_milliseconds
        = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
    stream << duration_milliseconds.count();
  }

  inline void Read(Time::TimePoint& time, std::istream& stream) {
    std::time_t unix_time;
    stream >> unix_time;
    if (stream) {
      time = Time::Clock::from_time_t(unix_time);
    }
  }

  inline void Read(Time::Duration& duration, std::istream& stream) {
    int64_t milliseconds;
    stream >> milliseconds;
    if (stream) {
      duration = std::chrono::duration_cast<Time::Duration>(
          std::chrono::milliseconds(milliseconds));
    }
  }
}

#endif  // DATA_TIME_H_
