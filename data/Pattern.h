#ifndef DATA_PATTERN_H_
#define DATA_PATTERN_H_

#include <bitset>
#include <iostream>

namespace Data {

class Pattern {
 public:
  static constexpr int Size = 10;

  Pattern() : Pattern(0) {}
  Pattern(int mask) { bits_ = mask; }

  bool operator==(const Pattern& pattern) const {
    return bits_ == pattern.bits_;
  }

  bool operator!=(const Pattern& pattern) const {
    return bits_ != pattern.bits_;
  }

  bool operator<(const Pattern& pattern) const {
    return mask() < pattern.mask();
  }

  bool get(int pos) const { return bits_.test(pos); }
  void set(int pos, bool bit) { bits_.set(pos, bit); }

  int mask() const { return static_cast<int>(bits_.to_ulong()); }

  void Write(std::ostream& stream) const {
    stream << bits_;
  }

  void Read(std::istream& stream) {
    Pattern tmp;
    stream >> tmp.bits_;
    if (stream) {
      std::swap(*this, tmp);
    }
  }

  void WriteCsv(std::ostream& stream) const {
    for (int i = 0; i < Size; i++) {
      if (bits_[i]) {
        stream << "O";
      } else {
        stream << ".";
      }
    }
  }

  template <typename Callback>
  static void IterateAllPatterns(Callback callback) {
    for (int i = 1; i < (1 << Size); i++) {
      callback(Pattern(i));
    }
  }

 private:
  std::bitset<Size> bits_;
};

}  // namespace Data

#endif  // DATA_PATTERN_H_
