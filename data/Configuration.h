#ifndef DATA_CONFIGURATION_H_
#define DATA_CONFIGURATION_H_

#include <cassert>
#include <fstream>
#include <gdk/gdk.h>
#include <iostream>
#include <sstream>

#include "Pattern.h"
#include "Time.h"

namespace Data {

class Configuration {
 public:
  static constexpr int PatternSize = Pattern::Size;

  Configuration() : feedback_(true), is_deadline_(false),
                    deadline_(std::chrono::seconds(10)),
                    delay_(std::chrono::seconds(2)),
                    training_session_size_(10),
                    test_session_size_(666) {
    key_bindings_[0] = GDK_KEY_a;
    key_bindings_[1] = GDK_KEY_s;
    key_bindings_[2] = GDK_KEY_d;
    key_bindings_[3] = GDK_KEY_f;
    key_bindings_[4] = GDK_KEY_g;
    key_bindings_[5] = GDK_KEY_h;
    key_bindings_[6] = GDK_KEY_j;
    key_bindings_[7] = GDK_KEY_k;
    key_bindings_[8] = GDK_KEY_l;
    key_bindings_[9] = GDK_KEY_semicolon;
  }

  bool feedback() const { return feedback_; }
  bool is_deadline() const { return is_deadline_; }
  Time::Duration deadline() const { return deadline_; }
  int deadline_ms() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
        deadline()).count();
  }
  Time::Duration delay() const { return delay_; }
  int delay_ms() const {
      return std::chrono::duration_cast<std::chrono::milliseconds>(
          delay()).count();
  }
  unsigned int KeyBindingLower(int id) const {
    assert(0 <= id and id < PatternSize);
    return gdk_keyval_to_lower(key_bindings_[id]);
  }
  unsigned int KeyBindingUpper(int id) const {
    assert(0 <= id and id < PatternSize);
    return gdk_keyval_to_upper(key_bindings_[id]);
  }

  std::string GetKeyText(int id) const {
    assert(0 <= id and id < PatternSize);
    auto get_key_label = [](unsigned int key) -> std::string {
      gchar* str = gdk_keyval_name(key);
      if (str == nullptr) {
        return "(?!?)";
      } else {
        return std::string(str);
      }
    };
    unsigned int lower = KeyBindingLower(id);
    unsigned int upper = KeyBindingUpper(id);
    return get_key_label(upper);
    /*
    std::string label = get_key_label(lower);
    if (lower != upper) {
      label += " / " + get_key_label(upper);
    }
    return label;
    */
  }

  int WhichPressed(unsigned int key) const {
    for (int i = 0; i < PatternSize; i++) {
      if (KeyBindingLower(i) == key or KeyBindingUpper(i) == key) {
        return i;
      }
    }
    return -1;
  }

  int training_session_size() const { return training_session_size_; }
  int test_session_size() const { return test_session_size_; }

  void SetFeedback(bool feedback) { feedback_ = feedback; }
  void SetIsDeadline(bool is_deadline) { is_deadline_ = is_deadline; }
  void SetDeadline(Time::Duration deadline) { deadline_ = deadline; }
  void SetDeadlineMs(int milliseconds) {
    deadline_ = std::chrono::milliseconds(milliseconds);
  }
  void SetDelay(Time::Duration delay) { delay_ = delay; }
  void SetDelayMs(int milliseconds) {
    delay_ = std::chrono::milliseconds(milliseconds);
  }
  void SetKeyBinding(int id, unsigned int key) {
    assert(0 <= id and id < PatternSize);
    key_bindings_[id] = gdk_keyval_to_lower(key);
  }
  void SetTrainingSessionSize(int size) { training_session_size_ = size; }
  void SetTestSessionSize(int size) { test_session_size_ = size; }

  void Write(std::ostream& stream) const {
    stream << feedback_ << " " << is_deadline_ << " ";
    Time::Write(deadline_, stream);
    stream << " ";
    Time::Write(delay_, stream);
    for (int i = 0; i < PatternSize; i++) {
      stream << " " << key_bindings_[i];
    }
    stream << " " << training_session_size_ << " " << test_session_size_;
  }

  void Read(std::istream& stream) {
    Configuration tmp;
    stream >> tmp.feedback_ >> tmp.is_deadline_;
    Time::Read(tmp.deadline_, stream);
    Time::Read(tmp.delay_, stream);
    for (int i = 0; i < PatternSize; i++) {
      stream >> tmp.key_bindings_[i];
    }
    stream >> training_session_size_ >> test_session_size_;
    if (stream) {
      std::swap(*this, tmp);
    }
  }

  void WriteCsv(std::ostream& stream) const {
    stream << "Feedback:" << "," << (feedback_ ? "on" : "off") << "\r\n";
    if (is_deadline_) {
      stream << "Deadline (milliseconds):" << ","
             << Time::ToMilliseconds(deadline_) << "\r\n";
    } else {
      stream << "Deadline:" << "," << "None" << "\r\n";
    }
    stream << "Delay between two samples (milliseconds):" << ","
           << Time::ToMilliseconds(delay_) << "\r\n";
    stream << "Key bindings:" << "\r\n";
    for (int i = 0; i < PatternSize; i++) {
      if (i) {
        stream << ",";
      }
      stream << i;
    }
    stream << "\r\n";
    for (int i = 0; i < PatternSize; i++) {
      if (i) {
        stream << ",";
      }
      stream << GetKeyText(i);
    }
    stream << "\r\n";
    stream << "Size of training session:" << ","
           << training_session_size_ << "\r\n";
    stream << "Size of normal session:" << ","
           << test_session_size_ << "\r\n";
  }

  static std::string TrainingSessionMessage() {
    std::ifstream input("training_session_message.txt");
    std::stringstream stream;
    stream << input.rdbuf();
    if (!input) {
      return "";
    }
    return stream.str();
  }

  static std::string TestSessionMessage() {
    std::ifstream input("test_session_message.txt");
    std::stringstream stream;
    stream << input.rdbuf();
    if (!input) {
      return "";
    }
    return stream.str();
  }

 private:
  bool feedback_;
  bool is_deadline_;
  Time::Duration deadline_;
  Time::Duration delay_;

  int training_session_size_;
  int test_session_size_;

  unsigned int key_bindings_[PatternSize];
};

}  // namespace Data

#endif  // DATA_CONFIGURATION_H_
