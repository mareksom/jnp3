#ifndef DATA_DATA_H_
#define DATA_DATA_H_

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string.h>
#include <string>

#include "Configuration.h"
#include "Session.h"
#include "Time.h"

namespace Data {

class Data {
 public:
  Data() : start_with_training_(true) {}

  std::string& nickname() { return nickname_; }
  const std::string& nickname() const { return nickname_; }

  int& age() { return age_; }
  int age() const { return age_; }

  std::string& sex() { return sex_; }
  const std::string& sex() const { return sex_; }

  Session& training() { return training_; }
  const Session& training() const { return training_; }

  Session& test() { return test_; }
  const Session& test() const { return test_; }

  bool start_with_training() const { return start_with_training_; }
  bool& start_with_training() { return start_with_training_; }

  Configuration& configuration() { return configuration_; }
  const Configuration& configuration() const { return configuration_; }

  void Save() const {
    if (filename_ == "") {
      throw std::runtime_error("No file to write.");
    }
    std::ofstream file(filename_);
    if (!file) {
      throw std::runtime_error(strerror(errno));
    }
    Write(file);
    if (!file) {
      throw std::runtime_error("Writing failed.");
    }
  }

  void SaveTo(const std::string& filename) {
    filename_ = filename;
    Save();
  }

  void SaveToCsv(const std::string& filename) const {
    std::ofstream file(filename);
    if (!file) {
      throw std::runtime_error(strerror(errno));
    }
    WriteCsv(file);
    if (!file) {
      throw std::runtime_error("Writing failed.");
    }
  }

  static Data ReadFrom(const std::string& filename) {
    Data tmp;
    std::ifstream file(filename);
    if (!file) {
      throw std::runtime_error(strerror(errno));
    }
    tmp.Read(file);
    if (!file) {
      throw std::runtime_error("The file is broken.");
    }
    tmp.filename_ = filename;
    return tmp;
  }

  void Write(std::ostream& stream) const {
    stream << nickname_ << "\n";
    stream << sex_ << "\n";
    stream << age_ << "\n";
    training_.Write(stream); stream << "\n";
    test_.Write(stream); stream << "\n";
    stream << start_with_training_ << "\n";
    configuration_.Write(stream);
    stream << "\n";
  }

  void Read(std::istream& stream) {
    Data tmp;
    std::getline(stream, tmp.nickname_);
    std::getline(stream, tmp.sex_);
    stream >> tmp.age_;
    tmp.training_.Read(stream);
    tmp.test_.Read(stream);
    stream >> tmp.start_with_training_;
    configuration_.Read(stream);
    if (stream) {
      std::swap(*this, tmp);
    }
  }

  void WriteCsv(std::ostream& stream) const {
    stream << "Nickname:," << nickname_ << "\r\n";
    stream << "Age:," << age_ << "\r\n";
    stream << "Sex:," << sex_ << "\r\n";
    stream << "\r\n";
    stream << "Configuration:" << "\r\n";
    configuration_.WriteCsv(stream);
    stream << "\r\n";
    stream << "Training session";
    if (start_with_training_) {
      stream << "(not complete):" << "\r\n";
    } else {
      stream << "(completed):" << "\r\n";
    }
    training_.WriteCsv(stream);
    stream << "\r\n";
    stream << "Normal session:" << "\r\n";
    test_.WriteCsv(stream);
    stream << "\r\n";
  }

 private:
  std::string filename_;

  std::string nickname_;
  int age_;
  std::string sex_;

  Session training_, test_;
  bool start_with_training_;

  Configuration configuration_;
};

}  // namespace Data

#endif  // DATA_DATA_H_
