#ifndef DATA_SESSION_H_
#define DATA_SESSION_H_

#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "Configuration.h"
#include "PatternResult.h"
#include "Time.h"

namespace Data {

class Session {
 public:
  Session() {
    ComputePatternsLeft();
  }

  Time::TimePoint start() const { return start_; }
  Time::TimePoint end() const { return end_; }

  int NumberOfResults() const { return pattern_results_.size(); }

  // Iterates over all pattern results.
  // Callback: PatternResult -> void
  template <typename Callback>
  void IteratePatternResults(Callback callback) const {
    for (const auto& pattern_result : pattern_results_) {
      callback(pattern_result);
    }
  }

  void SetStart(const Time::TimePoint& time) {
    start_ = time;
  }

  void SetEnd(const Time::TimePoint& time) {
    end_ = time;
  }

  void AddPatternResult(const PatternResult& pattern_result) {
    pattern_results_.push_back(pattern_result);
    patterns_left_.erase(pattern_result.pattern());
  }

  void Write(std::ostream& stream) const {
    Time::Write(start_, stream); stream << "\n";
    Time::Write(end_, stream); stream << "\n";
    stream << pattern_results_.size() << "\n";
    for (const auto& pattern_result : pattern_results_) {
      pattern_result.Write(stream);
      stream << "\n";
    }
  }

  void Read(std::istream& stream) {
    Session tmp;
    Time::Read(tmp.start_, stream);
    Time::Read(tmp.end_, stream);
    int number_of_pattern_results = 0;
    stream >> number_of_pattern_results;
    for (int i = 0; i < number_of_pattern_results; i++) {
      PatternResult pattern_result;
      pattern_result.Read(stream);
      tmp.pattern_results_.push_back(pattern_result);
    }
    if (stream) {
      tmp.ComputePatternsLeft();
      std::swap(tmp, *this);
    }
  }

  void WriteCsv(std::ostream& stream) const {
    stream << "Start of session:" << "," << Time::ToString(start_) << "\r\n";
    stream << "End of session:" << "," << Time::ToString(end_) << "\r\n";
    stream << "Number of patterns solved:" << ","
           << pattern_results_.size() << "\r\n";
    for (const PatternResult& pattern_result : pattern_results_) {
      pattern_result.WriteCsv(stream);
    }
  }

  Pattern RandomPattern() {
    if (patterns_left_.empty()) {
      ComputePatternsLeft();
    }
    assert(!patterns_left_.empty());
    auto it = patterns_left_.begin();
    std::advance(it, rand() % patterns_left_.size());
    return *it;
  }

 private:
  void ComputePatternsLeft() {
    std::map<Pattern, int> sizes;
    IteratePatternResults(
        [&sizes](const PatternResult& pattern_result) -> void {
          sizes[pattern_result.pattern()]++;
        });
    patterns_left_.clear();
    int minimal_size = 0;
    Pattern::IterateAllPatterns(
        [this, &sizes, &minimal_size](const Pattern& pattern) -> void {
          int size = sizes[pattern];
          if (size < minimal_size) {
            patterns_left_.clear();
            minimal_size = size;
          }
          if (size == minimal_size) {
            patterns_left_.insert(pattern);
          }
        });
  }

  Time::TimePoint start_, end_;
  std::vector<PatternResult> pattern_results_;

  std::set<Pattern> patterns_left_;
};

}  // namespace Data

#endif  // DATA_SESSION_H_
