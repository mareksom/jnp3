#ifndef LOADING_DATA_H_
#define LOADING_DATA_H_

#include <functional>
#include <glibmm/dispatcher.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/eventbox.h>
#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <memory>
#include <thread>

#include "Configuration.h"
#include "data/Data.h"

class LoadingData : public Gtk::EventBox {
 public:
  LoadingData(Data::Data* data);
  virtual ~LoadingData();

  sigc::signal<void> signal_done();

 protected:
  bool on_key_press_event(GdkEventKey* key_event) override;

  void NewSession();

 private:
  const std::string password_;
  int matched_;

  enum class State {
    Idle,
    LoadingData,
    Configuration,
  };

  State state_;

  void LoadData();
  void on_notification();
  std::unique_ptr<std::thread> worker_;
  Glib::Dispatcher dispatcher_;

  std::unique_ptr<Configuration> configuration_;

  Gtk::Button load_button_;
  Gtk::Button new_button_;
  Gtk::Label message_;
  Gtk::Grid grid_;

  Data::Data* data_;
  std::string error_message_;

  sigc::signal<void> signal_done_;
};

#endif  // LOADING_DATA_H_
