#include "Duck.h"

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <cstdlib>
#include <gdk/gdk.h>
#include <iostream>

namespace {

bool is_duck;
Mix_Music* duck;

}  // namespace

void Duck::Init(int argc, char** argv) {
  is_duck = true;
  auto parse = [](std::string arg) -> void {
    const std::string flag = "--sound=";
    if (arg.size() <= flag.size()) {
      return;
    }
    bool ok = true;
    for (int i = 0; i < (int) flag.size(); i++) {
      if (flag[i] != arg[i]) {
        ok = false;
        break;
      }
    }
    if (ok) {
      arg = arg.substr(flag.size());
      if (arg == "beep") {
        is_duck = false;
      } else if (arg == "duck") {
        is_duck = true;
      }
    }
  };
  for (int i = 1; i < argc; i++) {
    parse(argv[i]);
  }
  if (is_duck) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
      std::cerr << "SDL_Init: " << SDL_GetError() << std::endl;
      exit(EXIT_FAILURE);
    }
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) != 0) {
      std::cerr << "Mix_OpenAudio: " << SDL_GetError() << std::endl;
      exit(EXIT_FAILURE);
    }
    duck = Mix_LoadMUS("duck_quack.wav");
    if (duck == nullptr) {
      std::cerr << "Mix_LoadMUS: " << SDL_GetError() << std::endl;
      exit(EXIT_FAILURE);
    }
  }
}

void Duck::Play() {
  if (is_duck) {
    if (Mix_PlayMusic(duck, 1) != 0) {
      std::cerr << "Mix_PlayMusic: " << SDL_GetError() << std::endl;
    }
  } else {
    gdk_beep();
  }
}

void Duck::Quit() {
  if (is_duck) {
    Mix_FreeMusic(duck);
    Mix_CloseAudio();
    SDL_Quit();
  }
}
