#ifndef RESULTS_LIST_H_
#define RESULTS_LIST_H_

#include <gtkmm-3.0/gtkmm/listbox.h>
#include <gtkmm-3.0/gtkmm/scrolledwindow.h>

#include "ResultsListRow.h"
#include "data/Session.h"

class ResultsList : public Gtk::ScrolledWindow {
 public:
  ResultsList(const Data::Session* session);
  virtual ~ResultsList();

  sigc::signal<void, const Data::PatternResult*>
      signal_pattern_result_selected();

 private:
  Gtk::ListBox list_box_;

  sigc::signal<void, const Data::PatternResult*>
      signal_pattern_result_selected_;
};

#endif  // RESULTS_LIST_H_
