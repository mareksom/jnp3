#ifndef RESULTS_VIEW_H_
#define RESULTS_VIEW_H_

#include <gtkmm-3.0/gtkmm/box.h>
#include <gtkmm-3.0/gtkmm/button.h>

#include "PatternResultView.h"
#include "ResultsList.h"
#include "data/Session.h"

class ResultsView : public Gtk::Box {
 public:
  ResultsView(const Data::Session* session);
  virtual ~ResultsView();

  sigc::signal<void> signal_done();

 private:
  Gtk::Box box_;
  Gtk::Button back_button_;
  ResultsList results_list_;
  PatternResultView pattern_result_view_;

  sigc::signal<void> signal_done_;
};

#endif  // RESULTS_VIEW_H_
