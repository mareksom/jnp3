#ifndef KEY_BINDINGS_H_
#define KEY_BINDINGS_H_

#include <gtkmm-3.0/gtkmm/eventbox.h>
#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <string>

#include "Light.h"
#include "data/Configuration.h"

class KeyBindings : public Gtk::EventBox {
 public:
  static constexpr int PatternSize = Data::Configuration::PatternSize;

  KeyBindings(Data::Configuration* configuration);
  virtual ~KeyBindings();

 protected:
  bool on_key_press_event(GdkEventKey* event) override;
  bool on_key_release_event(GdkEventKey* event) override;

 private:
  void UpdateKeyLabel(int id);

  Data::Configuration* configuration_;

  Light lights_[PatternSize];
  int key_for_light_;
  Gtk::Label key_label_[PatternSize];
  Gtk::Grid grid_;
};

#endif  // KEY_BINDINGS_H_
