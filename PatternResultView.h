#ifndef PATTERN_RESULT_VIEW_H_
#define PATTERN_RESULT_VIEW_H_

#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/scrolledwindow.h>

#include "data/PatternResult.h"

class PatternResultView : public Gtk::ScrolledWindow {
 public:
  PatternResultView();
  virtual ~PatternResultView();

  void SetPatternResult(const Data::PatternResult* pattern_result);

 private:
  const Data::PatternResult* pattern_result_;

  Gtk::Grid* grid_;
};

#endif  // PATTERN_RESULT_VIEW_H_
