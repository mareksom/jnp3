#ifndef TRAINING_SESSION_H_
#define TRAINING_SESSION_H_

#include <gtkmm-3.0/gtkmm/bin.h>

#include "Solving.h"
#include "TextMessage.h"
#include "data/Configuration.h"
#include "data/Data.h"
#include "data/Session.h"

class TrainingSession : public Gtk::Bin {
 public:
  TrainingSession(Data::Data* data);
  virtual ~TrainingSession();

  enum class Choice {
    Next,
    Back,
  };

  sigc::signal<void, Choice> signal_done();

 private:
  void SetupSolving();
  void Start();

  Data::Session* training_session_;
  Data::Configuration* configuration_;

  Solving solving_;

  std::unique_ptr<TextMessage> text_message_;

  sigc::signal<void, Choice> signal_;
};

#endif  // TRAINING_SESSION_H_
