#include "Configuration.h"

#include <gtkmm-3.0/gtkmm/filechooserdialog.h>
#include <sstream>
#include <string>

#include "AllowLambda.h"

Configuration::Configuration(Data::Data* data)
    : Gtk::Grid(),
      data_(data),
      back_button_("Back"),
      ok_button_("Create session"),
      title_label_("Configure the session"),
      feedback_label_("Feedback: "),
      is_deadline_label_("Deadline: "),
      deadline_label_("Deadline: "),
      deadline_(Gtk::ORIENTATION_HORIZONTAL),
      deadline_value_label_("milliseconds"),
      delay_label_("Delay: "),
      delay_(Gtk::ORIENTATION_HORIZONTAL),
      delay_value_label_("milliseconds"),
      key_bindings_label_("Keys: "),
      key_bindings_(&(data->configuration())),
      training_session_size_label_("Size of training session: "),
      training_session_size_explanation_("(number of patterns)"),
      test_session_size_label_("Size of normal session: "),
      test_session_size_explanation_(
          "(number of repetitions of 1023 patterns)") {
  // Margin settings.
  set_margin_top(20);
  set_margin_bottom(20);
  set_margin_left(20);
  set_margin_right(20);
  set_column_spacing(20);
  set_row_spacing(20);

  // Delay widget settings.
  constexpr int DelayMaxSeconds = 10;
  delay_.signal_value_changed().connect(
      [this]() -> void {
        double milliseconds = 1000 * delay_.get_value();
        int rounded_milliseconds = static_cast<int>(std::round(milliseconds));
        delay_value_label_.set_text(
            std::to_string(rounded_milliseconds) + " milliseconds");
      });
  delay_.set_draw_value(false);
  delay_.set_range(0.1, DelayMaxSeconds);
  delay_.set_increments(0.1, 1);
  delay_.set_size_request(300, -1);
  for (int i = 0; i <= DelayMaxSeconds; i++) {
    delay_.add_mark(i, Gtk::POS_BOTTOM, std::to_string(i));
  }

  // Deadline widget settings.
  constexpr int DeadlineMaxSeconds = 10;
  deadline_.signal_value_changed().connect(
      [this]() -> void {
        double milliseconds = 1000 * deadline_.get_value();
        int rounded_milliseconds = static_cast<int>(std::round(milliseconds));
        deadline_value_label_.set_text(
            std::to_string(rounded_milliseconds) + " milliseconds");
      });
  deadline_.set_draw_value(false);
  deadline_.set_range(0.1, DeadlineMaxSeconds);
  deadline_.set_increments(0.1, 1);
  deadline_.set_size_request(300, -1);
  for (int i = 0; i <= DeadlineMaxSeconds; i++) {
    deadline_.add_mark(i, Gtk::POS_BOTTOM, std::to_string(i));
  }

  attach(back_button_,                       0, 0, 1, 1);
  attach(title_label_,                       1, 0, 3, 1);
  attach(feedback_label_,                    0, 1, 1, 1);
  attach(feedback_,                          1, 1, 1, 1);
  attach(is_deadline_label_,                 0, 2, 1, 1);
  attach(is_deadline_,                       1, 2, 1, 1);
  attach(deadline_label_,                    0, 3, 1, 1);
  attach(deadline_,                          1, 3, 2, 1);
  attach(deadline_value_label_,              3, 3, 1, 1);
  attach(delay_label_,                       0, 4, 1, 1);
  attach(delay_,                             1, 4, 2, 1);
  attach(delay_value_label_,                 3, 4, 1, 1);
  attach(key_bindings_label_,                0, 5, 1, 1);
  attach(key_bindings_,                      1, 5, 3, 1);
  attach(training_session_size_label_,       0, 6, 1, 1);
  attach(training_session_size_,             1, 6, 1, 1);
  attach(training_session_size_explanation_, 2, 6, 1, 1);
  attach(test_session_size_label_,           0, 7, 1, 1);
  attach(test_session_size_,                 1, 7, 1, 1);
  attach(test_session_size_explanation_,     2, 7, 1, 1);
  attach(ok_button_,                         1, 8, 1, 1);

  back_button_.signal_clicked().connect(
      [this]() -> void {
        signal_.emit(Event::Back);
      });

  ok_button_.signal_clicked().connect(
      [this]() -> void {
        try {
          Gtk::FileChooserDialog dialog(
              "Save to", Gtk::FILE_CHOOSER_ACTION_SAVE);
          dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
          dialog.add_button("_Save", Gtk::RESPONSE_OK);

          auto filter = Gtk::FileFilter::create();
          filter->set_name("MS files");
          filter->add_pattern("*.ms");
          dialog.add_filter(filter);

          int result = dialog.run();

          if (result == Gtk::RESPONSE_OK) {
            data_->SaveTo(dialog.get_filename());
            signal_.emit(Event::Done);
          }
        } catch (std::runtime_error& e) {
        }
      });

  feedback_.set_active(data_->configuration().feedback());
  is_deadline_.set_active(data_->configuration().is_deadline());
  deadline_.set_value(data_->configuration().deadline_ms() / 1000.0);
  delay_.set_value(data_->configuration().delay_ms() / 1000.0);
  training_session_size_.set_text(
      std::to_string(data_->configuration().training_session_size()));
  test_session_size_.set_text(
      std::to_string(data_->configuration().test_session_size()));

  feedback_.property_active().signal_changed().connect(
      [this]() -> void {
        data_->configuration().SetFeedback(feedback_.get_active());
      });
  is_deadline_.property_active().signal_changed().connect(
      [this]() -> void {
        data_->configuration().SetIsDeadline(is_deadline_.get_active());
      });
  deadline_.signal_value_changed().connect(
      [this]() -> void {
        double milliseconds = 1000 * deadline_.get_value();
        int rounded_milliseconds = static_cast<int>(std::round(milliseconds));
        data_->configuration().SetDeadlineMs(rounded_milliseconds);
      });
  delay_.signal_value_changed().connect(
      [this]() -> void {
        double milliseconds = 1000 * delay_.get_value();
        int rounded_milliseconds = static_cast<int>(std::round(milliseconds));
        data_->configuration().SetDelayMs(rounded_milliseconds);
      });

  training_session_size_.signal_changed().connect(
      [this]() -> void {
        std::string text = training_session_size_.get_buffer()->get_text();
        bool ok = true;
        for (char c : text) {
          if (!('0' <= c and c <= '9')) {
            ok = false;
            break;
          }
        }
        if ((int) text.length() > 5) {
          ok = false;
        }
        if (!text.empty() and text[0] == '0') {
          ok = false;
        }
        if (ok) {
          std::stringstream stream(text);
          int size = 0;
          stream >> size;
          data_->configuration().SetTrainingSessionSize(size);
        } else {
          training_session_size_.set_text(
              std::to_string(data_->configuration().training_session_size()));
        }
      });

  test_session_size_.signal_changed().connect(
      [this]() -> void {
        std::string text = test_session_size_.get_buffer()->get_text();
        bool ok = true;
        for (char c : text) {
          if (!('0' <= c and c <= '9')) {
            ok = false;
            break;
          }
        }
        if ((int) text.length() > 5) {
          ok = false;
        }
        if (!text.empty() and text[0] == '0') {
          ok = false;
        }
        if (ok) {
          std::stringstream stream(text);
          int size = 0;
          stream >> size;
          data_->configuration().SetTestSessionSize(size);
        } else {
          test_session_size_.set_text(
              std::to_string(data_->configuration().test_session_size()));
        }
      });

  show_all();
}

Configuration::~Configuration() {}

sigc::signal<void, Configuration::Event> Configuration::signal_event() {
  return signal_;
}
