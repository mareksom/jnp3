#ifndef MAIN_WINDOW_H_
#define MAIN_WINDOW_H_

#include <gtkmm-3.0/gtkmm/window.h>
#include <memory>

#include "ChoosingResults.h"
#include "ChoosingWhatToDo.h"
#include "LoadingData.h"
#include "Nickname.h"
#include "TestSession.h"
#include "TrainingSession.h"
#include "data/Data.h"

class MainWindow : public Gtk::Window {
 public:
  MainWindow();
  virtual ~MainWindow();

 private:
  void LoadData();
  std::unique_ptr<LoadingData> loading_data_;

  void ChooseWhatToDo();
  std::unique_ptr<ChoosingWhatToDo> choosing_what_to_do_;

  void RunASession();

  void RunNickname();
  std::unique_ptr<Nickname> nickname_;

  void RunTrainingSession();
  std::unique_ptr<TrainingSession> training_session_;

  void RunTestSession();
  std::unique_ptr<TestSession> test_session_;

  void RunChoosingResults();
  std::unique_ptr<ChoosingResults> choosing_results_;

  Data::Data data_;
};

#endif  // MAIN_WINDOW_H_
