#include "KeyBindings.h"

#include <cassert>
#include <gdk/gdk.h>

#include "AllowLambda.h"

KeyBindings::KeyBindings(Data::Configuration* configuration)
    : configuration_(configuration), key_for_light_(-1) {
  add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK);
  set_can_focus(true);
  for (int i = 0; i < PatternSize; i++) {
    grid_.attach(lights_[i], i, 0, 1, 1);
    grid_.attach(key_label_[i], i, 1, 1, 1);
    lights_[i].SetColor(Light::Color::None);
    lights_[i].show();
    key_label_[i].show();
    UpdateKeyLabel(i);
    lights_[i].add_events(Gdk::ENTER_NOTIFY_MASK | Gdk::LEAVE_NOTIFY_MASK
                          | Gdk::BUTTON_PRESS_MASK);

    lights_[i].signal_enter_notify_event().connect(
        [this, i](GdkEventCrossing*) -> bool {
          if (key_for_light_ != i) {
            lights_[i].SetColor(Light::Color::Yellow);
          }
          return true;
        });
    lights_[i].signal_leave_notify_event().connect(
        [this, i](GdkEventCrossing*) -> bool {
          if (key_for_light_ != i) {
            lights_[i].SetColor(Light::Color::None);
          }
          return true;
        });
    lights_[i].signal_button_press_event().connect(
        [this, i](GdkEventButton*) -> bool {
          if (key_for_light_ != -1) {
            lights_[key_for_light_].SetColor(Light::Color::None);
          }
          grab_focus();
          key_for_light_ = i;
          lights_[i].SetColor(Light::Color::Green);
          return true;
        });
  }

  add(grid_);
  grid_.show();
}

KeyBindings::~KeyBindings() {}

bool KeyBindings::on_key_press_event(GdkEventKey* event) {
  if (key_for_light_ != -1) {
    if (event->keyval != GDK_KEY_Escape) {
      assert(0 <= key_for_light_ and key_for_light_ < PatternSize);
      configuration_->SetKeyBinding(key_for_light_, event->keyval);
      lights_[key_for_light_].SetColor(Light::Color::None);
      UpdateKeyLabel(key_for_light_);
      key_for_light_ = -1;
      return true;
    }
  } else {
    int which = configuration_->WhichPressed(event->keyval);
    if (which != -1) {
      assert(0 <= which and which < PatternSize);
      lights_[which].SetColor(Light::Color::Blue);
      return true;
    }
  }
  return false;
}

bool KeyBindings::on_key_release_event(GdkEventKey* event) {
  if (key_for_light_ == -1) {
    int which = configuration_->WhichPressed(event->keyval);
    if (which != -1) {
      assert(0 <= which and which < PatternSize);
      lights_[which].SetColor(Light::Color::None);
      return true;
    }
  }
  return false;
}

void KeyBindings::UpdateKeyLabel(int id) {
  key_label_[id].set_text(configuration_->GetKeyText(id));
}
