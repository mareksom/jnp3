#include "TextMessage.h"

#include "AllowLambda.h"

TextMessage::TextMessage(const std::string& message)
    : v_box_(Gtk::ORIENTATION_VERTICAL),
      message_(),
      ok_button_("Continue") {
  set_margin_top(50);
  set_margin_bottom(50);
  set_margin_left(50);
  set_margin_right(50);

  auto text_buffer = Gtk::TextBuffer::create();
  text_buffer->set_text(message);
  message_.set_buffer(text_buffer);
  message_.set_editable(false);

  v_box_.pack_start(message_, Gtk::PACK_EXPAND_WIDGET);
  v_box_.pack_end(ok_button_, Gtk::PACK_SHRINK);

  ok_button_.signal_clicked().connect(
      [this]() -> void {
        signal_done_.emit();
      });

  message_.show();
  ok_button_.show();
  v_box_.show();

  add(v_box_);
}

sigc::signal<void> TextMessage::signal_done() {
  return signal_done_;
}
