#ifndef LIGHT_SET_H_
#define LIGHT_SET_H_

#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <string>

#include "Countdown.h"
#include "Light.h"
#include "data/Configuration.h"
#include "data/Pattern.h"
#include "data/PatternResult.h"
#include "data/Session.h"

class LightSet : public Gtk::Grid {
 public:
  static constexpr int Size = Data::Pattern::Size;

  LightSet(const std::string& title, Data::Session* session,
           const Data::Configuration* configuration);
  virtual ~LightSet();

  Light::Color GetColor(int light_id) const;
  void SetLight(int light_id, Light::Color color);
  void SetLights(Light::Color color);
  void SetLights(const Data::Pattern& pattern);

  // Return true if the round started successfully.
  bool RunRound();
  // Return true if it was a good time to trigger anything.
  bool Trigger(int light_id);
  bool RoundIsDone() const;

  void Reset();

  sigc::signal<void> signal_end_of_round();

 protected:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& context) override;

 private:
  enum class State {
    Idle,
    Round,
  };

  State state_;
  Data::Session* session_;
  const Data::Configuration* configuration_;

  Gtk::Label title_label_;
  Countdown countdown_;
  Light lights_[Size];
  Gtk::Label light_labels_[Size];

  void RoundSummary();
  void on_timeout();

  Data::PatternResult pattern_result_;

  sigc::signal<void> signal_end_of_round_;
};

#endif  // LIGHT_SET_H_
