#include "ResultsListRow.h"

ResultsListRow::ResultsListRow(const Data::PatternResult* pattern_result)
    : pattern_result_(pattern_result) {
  const Data::Pattern& pattern = pattern_result->pattern();

  for (int i = 0; i < PatternSize; i++) {
    lights_[i].Resize(20);
    grid_.attach(lights_[i], i, 0, 1, 1);
    if (pattern.get(i)) {
      lights_[i].SetColor(Light::Color::Yellow);
    } else {
      lights_[i].SetColor(Light::Color::None);
    }
    lights_[i].show();
  }

  add(grid_);
  grid_.show();
}

ResultsListRow::~ResultsListRow() {}

const Data::PatternResult* ResultsListRow::pattern_result() const {
  return pattern_result_;
}
