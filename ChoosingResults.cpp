#include "ChoosingResults.h"

#include <gtkmm-3.0/gtkmm/filechooserdialog.h>

#include "data/Session.h"

ChoosingResults::ChoosingResults(const Data::Data* data)
    : data_(data),
      back_button_("Back"),
      training_button_("Training Session"),
      test_button_("Normal Session"),
      export_to_csv_button_("Export to CSV"),
      results_view_(nullptr) {
  grid_.set_margin_top(20);
  grid_.set_margin_bottom(20);
  grid_.set_margin_left(20);
  grid_.set_margin_right(20);
  grid_.set_row_spacing(20);
  grid_.set_column_spacing(20);

  grid_.attach(back_button_,          0, 0, 1, 1);
  grid_.attach(training_button_,      1, 0, 1, 1);
  grid_.attach(test_button_,          2, 0, 1, 1);
  grid_.attach(export_to_csv_button_, 3, 0, 1, 1);

  back_button_.signal_clicked().connect(
      [this]() -> void {
        signal_done_.emit();
      });

  auto SessionSlot = [this](const Data::Session* session) {
    return [this, session]() -> void {
             remove();
             grid_.hide();
             results_view_
                 = std::unique_ptr<ResultsView>(new ResultsView(session));
             add(*results_view_);
             results_view_->show();
             results_view_->signal_done().connect(
                 [this]() -> void {
                   remove();
                   results_view_.reset();
                   add(grid_);
                   grid_.show();
                 });
           };
  };

  training_button_.signal_clicked().connect(SessionSlot(&(data_->training())));
  test_button_.signal_clicked().connect(SessionSlot(&(data_->test())));

  export_to_csv_button_.signal_clicked().connect(
      [this]() -> void {
        ExportToCsv();
      });

  add(grid_);
  back_button_.show();
  training_button_.show();
  test_button_.show();
  export_to_csv_button_.show();
  grid_.show();
}

ChoosingResults::~ChoosingResults() {}

sigc::signal<void> ChoosingResults::signal_done() {
  return signal_done_;
}

void ChoosingResults::ExportToCsv() {
  Gtk::FileChooserDialog dialog("Save to", Gtk::FILE_CHOOSER_ACTION_SAVE);

  dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("_Save", Gtk::RESPONSE_OK);

  auto filter = Gtk::FileFilter::create();
  filter->set_name("CSV files");
  filter->add_mime_type("text/csv");
  dialog.add_filter(filter);

  int result = dialog.run();

  if (result == Gtk::RESPONSE_OK) {
    const std::string& filename = dialog.get_filename();
    try {
      data_->SaveToCsv(filename);
    } catch (const std::runtime_error& e) {
    }
  }
}
