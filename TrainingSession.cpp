#include "TrainingSession.h"

#include <cassert>

TrainingSession::TrainingSession(Data::Data* data)
    : training_session_(&(data->training())),
      configuration_(&data->configuration()),
      solving_("Training session", training_session_, configuration_),
      text_message_(nullptr) {

  solving_.signal_event().connect(
      [this](Solving::Event event) -> void {
        if (event == Solving::Event::EndOfRound) {
          if (training_session_->NumberOfResults()
              >= configuration_->training_session_size()) {
            signal_.emit(Choice::Next);
          } else {
            solving_.StartRound();
          }
        } else if (event == Solving::Event::Escape) {
          signal_.emit(Choice::Back);
        } else {
          assert(false);
        }
      });

  if (training_session_->NumberOfResults() == 0) {
    text_message_ = std::unique_ptr<TextMessage>(
        new TextMessage(Data::Configuration::TrainingSessionMessage()));
    add(*text_message_);
    text_message_->signal_done().connect(
        [this]() -> void {
          text_message_.reset();
          SetupSolving();
        });
    text_message_->show();
  } else {
    SetupSolving();
  }
}

TrainingSession::~TrainingSession() {}

sigc::signal<void, TrainingSession::Choice> TrainingSession::signal_done() {
  return signal_;
}

void TrainingSession::SetupSolving() {
  Glib::signal_timeout().connect_once(
    [this]() -> void {
      add(solving_);
      solving_.show();
      solving_.grab_focus();
      solving_.StartRound();
    }, 100);
}

void TrainingSession::Start() {
  solving_.StartRound();
}
