#include "Countdown.h"

#include <algorithm>

#include "AllowLambda.h"

Countdown::Countdown()
    : is_running_(false),
      signal_timeout_(Glib::signal_timeout()) {
  SetText(-1);
}

Countdown::~Countdown() {
  Stop();
}

void Countdown::SetTo(int milliseconds) {
  start_point_ = Time::Now();
  end_point_ = start_point_ + std::chrono::milliseconds(milliseconds);
  main_connection_ = signal_timeout_.connect(
      [this]() -> bool {
        Stop();
        signal_.emit();
        return false;
      }, milliseconds);
  small_connection_ = signal_timeout_.connect(
      [this]() -> bool {
        UpdateTimeLeft();
        return true;
      }, 50);
  is_running_ = true;
  UpdateTimeLeft();
}

void Countdown::Stop() {
  if (is_running_) {
    main_connection_.disconnect();
    small_connection_.disconnect();
    UpdateTimeLeft();
    is_running_ = false;
  }
}

void Countdown::Reset() {
  Stop();
  SetText(-1);
}

sigc::signal<void> Countdown::signal_timeout() {
  return signal_;
}

void Countdown::UpdateTimeLeft() {
  if (is_running_) {
    int milliseconds_left = std::max(0,
        static_cast<int>(std::chrono::duration_cast<std::chrono::milliseconds>(
            end_point_ - Time::Now()).count()));
    SetText(milliseconds_left);
  } else {
    SetText(-1);
  }
}

void Countdown::SetText(int milliseconds) {
  if (milliseconds == -1) {
    set_text("Countdown: +oo");
  } else {
    set_text("Countdown: " + std::to_string(milliseconds) + "ms");
  }
}
