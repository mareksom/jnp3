#include "PatternResultView.h"

#include <cassert>
#include <gtkmm-3.0/gtkmm/label.h>

#include "data/Time.h"

PatternResultView::PatternResultView()
    : pattern_result_(nullptr),
      grid_(nullptr) {
  set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
}

PatternResultView::~PatternResultView() {}

void PatternResultView::SetPatternResult(
    const Data::PatternResult* pattern_result) {
  pattern_result_ = pattern_result;
  if (grid_ != nullptr) {
    remove();
    grid_ = nullptr;
  }
  if (pattern_result != nullptr) {
    pattern_result_ = pattern_result;
    grid_ = Gtk::manage(new Gtk::Grid());
    add(*grid_);

    grid_->set_margin_top(20);
    grid_->set_margin_bottom(20);
    grid_->set_margin_left(20);
    grid_->set_margin_right(20);
    grid_->set_row_spacing(20);
    grid_->set_column_spacing(20);

    int y = 0;

    grid_->attach(*Gtk::manage(new Gtk::Label("Pattern started: ")),
        0, y, 1, 1);
    grid_->attach(*Gtk::manage(new Gtk::Label(Time::ToString(
        pattern_result->start()))), 1, y++, 1, 1);

    pattern_result->IterateKeys(
        [this, &y](const Time::Duration& delta, int key) -> void {
          grid_->attach(*Gtk::manage(new Gtk::Label("+" + std::to_string(
              Time::ToMilliseconds(delta)) + " ms")), 1, y, 1, 1);
          grid_->attach(*Gtk::manage(new Gtk::Label("Light " + std::to_string(
              key))), 2, y++, 1, 1);
        });

    grid_->attach(*Gtk::manage(new Gtk::Label("Pattern ended: ")),
        0, y, 1, 1);
    grid_->attach(*Gtk::manage(new Gtk::Label(Time::ToString(
        pattern_result->end()))), 1, y++, 1, 1);

    grid_->show_all();
  }
}
