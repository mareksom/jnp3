#include "TestSession.h"

#include <cassert>

TestSession::TestSession(Data::Data* data)
    : test_session_(&data->test()),
      configuration_(&data->configuration()),
      solving_("", &(data->test()), &(data->configuration())) {

  solving_.signal_event().connect(
      [this](Solving::Event event) -> void {
        if (event == Solving::Event::EndOfRound) {
          if (End()) {
            signal_.emit(Choice::End);
          } else {
            solving_.StartRound();
          }
        } else if (event == Solving::Event::Escape) {
          signal_.emit(Choice::Back);
        } else {
          assert(false);
        }
      });

  if (test_session_->NumberOfResults() == 0) {
    text_message_ = std::unique_ptr<TextMessage>(
        new TextMessage(Data::Configuration::TestSessionMessage()));
    add(*text_message_);
    text_message_->signal_done().connect(
        [this]() -> void {
          text_message_.reset();
          SetupSolving();
        });
    text_message_->show();
  } else {
    if (End()) {
      Glib::signal_timeout().connect_once(
          [this]() -> void {
            signal_.emit(Choice::End);
          }, 100);
    } else {
      SetupSolving();
    }
  }
}

TestSession::~TestSession() {}

sigc::signal<void, TestSession::Choice> TestSession::signal_done() {
  return signal_;
}

void TestSession::SetupSolving() {
  Glib::signal_timeout().connect_once(
      [this]() -> void {
        add(solving_);
        solving_.show();
        solving_.grab_focus();
        solving_.StartRound();
      }, 100);
}

void TestSession::Start() {
  solving_.StartRound();
}

bool TestSession::End() const {
  return test_session_->NumberOfResults()
      >= configuration_->test_session_size() * 1023;
}
