#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/entry.h>
#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <gtkmm-3.0/gtkmm/scale.h>
#include <gtkmm-3.0/gtkmm/switch.h>

#include "KeyBindings.h"
#include "data/Data.h"
#include "data/Time.h"

class Configuration : public Gtk::Grid {
 public:
  Configuration(Data::Data* data);
  virtual ~Configuration();

  enum class Event {
    Back,
    Done,
  };

  sigc::signal<void, Event> signal_event();

 private:
  Data::Data* data_;

  Gtk::Button back_button_;
  Gtk::Button ok_button_;
  Gtk::Label title_label_;

  Gtk::Label feedback_label_;
  Gtk::Switch feedback_;

  Gtk::Label is_deadline_label_;
  Gtk::Switch is_deadline_;

  Gtk::Label deadline_label_;
  Gtk::Scale deadline_;
  Gtk::Label deadline_value_label_;

  Gtk::Label delay_label_;
  Gtk::Scale delay_;
  Gtk::Label delay_value_label_;

  Gtk::Label key_bindings_label_;
  KeyBindings key_bindings_;

  Gtk::Label training_session_size_label_;
  Gtk::Entry training_session_size_;
  Gtk::Label training_session_size_explanation_;

  Gtk::Label test_session_size_label_;
  Gtk::Entry test_session_size_;
  Gtk::Label test_session_size_explanation_;

  sigc::signal<void, Event> signal_;
};

#endif  // CONFIGURATION_H_
