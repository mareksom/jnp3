#include "ResultsList.h"

#include <cassert>

#include "AllowLambda.h"

ResultsList::ResultsList(const Data::Session* session) {
  set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
  add(list_box_);
  session->IteratePatternResults(
      [this](const Data::PatternResult& pattern_result) -> void {
        ResultsListRow* row = Gtk::manage(new ResultsListRow(&pattern_result));
        list_box_.append(*row);
        row->show();
      });
  list_box_.show();

  list_box_.signal_row_selected().connect(
      [this](Gtk::ListBoxRow* list_box_row) -> void {
        if (list_box_row == nullptr) {
        } else {
          ResultsListRow* row = dynamic_cast<ResultsListRow*>(list_box_row);
          assert(row != nullptr);
          signal_pattern_result_selected_.emit(row->pattern_result());
        }
      });
}

ResultsList::~ResultsList() {}

sigc::signal<void, const Data::PatternResult*>
    ResultsList::signal_pattern_result_selected() {
  return signal_pattern_result_selected_;
}
