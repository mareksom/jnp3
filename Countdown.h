#ifndef COUNTDOWN_H_
#define COUNTDOWN_H_

#include <functional>
#include <glibmm/main.h>
#include <gtkmm-3.0/gtkmm/label.h>

#include "data/Time.h"

class Countdown : public Gtk::Label {
 public:
  Countdown();
  virtual ~Countdown();

  void SetTo(int milliseconds);
  void Stop();
  void Reset();

  sigc::signal<void> signal_timeout();

 private:
  void UpdateTimeLeft();
  void SetText(int milliseconds);  // -1 means +infty.

  bool is_running_;
  Time::TimePoint start_point_, end_point_;
  Glib::SignalTimeout signal_timeout_;
  sigc::connection main_connection_;
  sigc::connection small_connection_;

  sigc::signal<void> signal_;
};

#endif  // COUNTDOWN_H_
