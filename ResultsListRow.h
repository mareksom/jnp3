#ifndef RESULTS_LIST_ROW_H_
#define RESULTS_LIST_ROW_H_

#include <gtkmm-3.0/gtkmm/grid.h>
#include <gtkmm-3.0/gtkmm/listboxrow.h>

#include "Light.h"
#include "data/Pattern.h"
#include "data/PatternResult.h"

class ResultsListRow : public Gtk::ListBoxRow {
 public:
  static constexpr int PatternSize = Data::Pattern::Size;

  ResultsListRow(const Data::PatternResult* pattern_result);
  virtual ~ResultsListRow();

  const Data::PatternResult* pattern_result() const;

 private:
  const Data::PatternResult* pattern_result_;

  Light lights_[PatternSize];
  Gtk::Grid grid_;
};

#endif  // RESULTS_LIST_ROW_H_
