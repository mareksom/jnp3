#ifndef CHOOSING_RESULTS_H_
#define CHOOSING_RESULTS_H_

#include <gtkmm-3.0/gtkmm/bin.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/grid.h>
#include <memory>

#include "ResultsView.h"
#include "data/Data.h"

class ChoosingResults : public Gtk::Bin {
 public:
  ChoosingResults(const Data::Data* data);
  virtual ~ChoosingResults();

  sigc::signal<void> signal_done();

 private:
  void ExportToCsv();

  const Data::Data* data_;

  Gtk::Grid grid_;
  Gtk::Button back_button_;
  Gtk::Button training_button_;
  Gtk::Button test_button_;
  Gtk::Button export_to_csv_button_;

  std::unique_ptr<ResultsView> results_view_;

  sigc::signal<void> signal_done_;
};

#endif  // CHOOSING_RESULTS_H_
